/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.demo.thrift.repo;

import com.demo.thrift.model.Story;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author phong
 */
@Repository
public class RedisRepository {

    private static final String KEY = "Story";

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    public void add(Story story) {
        redisTemplate.opsForHash().putIfAbsent(KEY, story.getId(), story);
    }

    public void update(Story story) {
        redisTemplate.opsForHash().put(KEY, story.getId(), story);
    }

    public void delete(int id) {
        redisTemplate.opsForHash().delete(KEY, id);
    }

    public Object findStory(int id) {
        return redisTemplate.opsForHash().get(KEY, id);
    }

    public Map<Object, Object> findAllStory() {
        return redisTemplate.opsForHash().entries(KEY);
    }
}
