///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.demo.thrift.repo;
//
//import com.demo.thrift.model.Story;
//import java.util.Map;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.redis.core.RedisTemplate;
//import org.springframework.stereotype.Repository;
//
///**
// *
// * @author phong
// */
//@Repository
//public class CustomizeaRepository {
//
//    private static final String KEY = "Story";
//
//    @Autowired
//    private RedisTemplate<String, Object> redisTemplate;
//
//    public void add(final Story story) {
//        redisTemplate.opsForHash().put(KEY, story.getId(), story);
//    }
//
//    public Story findStory(String id) {
//        return (Story) redisTemplate.opsForHash().get(KEY, id);
//    }
//
//    public Map<Object, Object> findAllStory() {
//        return redisTemplate.opsForHash().entries(KEY);
//    }
//}
