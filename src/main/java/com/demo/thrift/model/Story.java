/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.demo.thrift.model;

/**
 *
 * @author phong
 */
public class Story {

    private int id;

    private String title;

    private String normalizeTitle;

    private String author;

    private String description;

    private boolean doneFlag;

    private String image;

    private Float rate = 0.0f;

    private String dataFrom;

    private String currentPage;

    public Story() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNormalizeTitle() {
        return normalizeTitle;
    }

    public void setNormalizeTitle(String normalizeTitle) {
        this.normalizeTitle = normalizeTitle;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isDoneFlag() {
        return doneFlag;
    }

    public void setDoneFlag(boolean doneFlag) {
        this.doneFlag = doneFlag;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Float getRate() {
        return rate;
    }

    public void setRate(Float rate) {
        this.rate = rate;
    }

    public String getDataFrom() {
        return dataFrom;
    }

    public void setDataFrom(String dataFrom) {
        this.dataFrom = dataFrom;
    }

    public String getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Story{" + "id=" + id + ", title=" + title + ", normalizeTitle=" + normalizeTitle + ", author=" + author + ", description=" + description + ", doneFlag=" + doneFlag + ", image=" + image + ", rate=" + rate + ", dataFrom=" + dataFrom + ", currentPage=" + currentPage + '}';
    }

}
