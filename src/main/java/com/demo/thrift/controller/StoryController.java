/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.demo.thrift.controller;

import com.demo.thrift.model.Story;
import com.demo.thrift.repo.RedisRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author phong
 */
@RestController
public class StoryController {

    @Autowired
    private RedisRepository redisRepository;

    @PostMapping(value = "/story", produces = "application/json")
    public String insert(@RequestBody Story story) {
        redisRepository.add(story);
        return "Ok";
    }

    @GetMapping(value = "/story/{id}", produces = "application/json")
    public String getByid(@PathVariable(name = "id") String id) {
        return redisRepository.findStory(Integer.parseInt(id)).toString();
    }

    @GetMapping(value = "/story-all", produces = "application/json")
    public String getAll() {
        return redisRepository.findAllStory().toString();
    }

}
